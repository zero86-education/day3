# Day3

- redux
- recoil
- 리액트를 위한 자바스크립트(Promise, async ~ await, 비동기)
- fetch 를 활용한 서버 통신(with json-server)

```
get 요청
/[resource명]
/[resource명]/{id}
/[resource명]?title={필터명}

post 요청
/[resource명]

put 요청
/[resource명]/{id}

delete 요청
/[resource명]/{id}
```