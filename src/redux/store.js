// redux 스토어 생성
import {configureStore} from '@reduxjs/toolkit';
import fourArithmeticOperationsSliceReducer from './slice/fourArithmeticOperationsSlice';

// 생성한 스토어 인스턴스를 export
// 이를, ReduxIndex 에서 Provider 컴포넌트 store 속성에 주입합니다.
export default configureStore({
    reducer: {
        fourArithmeticOperationsSliceReducer // 생성한 리듀서를 지정
    },
});


// 옛날
// import { createStore } from 'redux';
// const store = createStore(); // 인자로 reducer 전달
//
// export default store;