// state, action, reducer 작성
import {createSlice} from '@reduxjs/toolkit';

export const fourArithmeticOperationsSlice = createSlice({
    name: 'fourArithmeticOperations',
    initialState: {
        num1: 1,
        num2: 1,
        result: 0,
    },
    reducers: {
        // 덧셈
        addition: (state) => {
            state.result = +state.num1 + +state.num2;
        },
        // 뺄셈
        subtraction: (state) => {
            state.result = state.num1 - state.num2;
        },
        // 곱센
        multiplication: (state) => {
            state.result = state.num1 * state.num2;
        },
        // 나눗셈
        division: (state) => {
            state.result = state.num1 / state.num2;
        },
        setNum1: (state, action) => {
            // {type: 'fourArithmeticOperations/setNum1', payload: '3'}
            console.log(action);
            state.num1 = action.payload;
        },
        setNum2: (state, action) => {
            // {type: 'fourArithmeticOperations/setNum1', payload: '2'}
            console.log(action);
            state.num2 = action.payload;
        },
    },
});

// 액션 생성자 함수
export const {
    addition,
    subtraction,
    multiplication,
    division,
    setNum1,
    setNum2,
} = fourArithmeticOperationsSlice.actions;

// 생성한 스토어에 reducer 속성에 주입
export default fourArithmeticOperationsSlice.reducer;