import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {setNum1, setNum2, addition, subtraction, multiplication, division} from './slice/fourArithmeticOperationsSlice';

// redux 를 사용해보자!

export default function ReduxEx() {
    // useSelector 훅으로 스토어 상태값을 받는다.
    // 여러번 사용한다고 특별히 성능이 떨어지지 않음
    const num1 = useSelector(state => state.fourArithmeticOperationsSliceReducer.num1);
    const num2 = useSelector(state => state.fourArithmeticOperationsSliceReducer.num2);
    const result = useSelector(state => state.fourArithmeticOperationsSliceReducer.result);

    const dispatch = useDispatch();  // 액션을 일으키는 dispatch()

    const handleNum1Change = (e) => {
        // num1 update
        dispatch(setNum1(e.target.value));
    };

    const handleNum2Change = (e) => {
        // num2 update
        dispatch(setNum2(e.target.value));
    };

    return (
        <div>
            <input type="number" value={num1} onChange={handleNum1Change}/>
            <input type="number" value={num2} onChange={handleNum2Change}/>
            <button onClick={() => dispatch(addition())}>덧셈</button>
            <button onClick={() => dispatch(subtraction())}>뺄셈</button>
            <button onClick={() => dispatch(multiplication())}>곱하기</button>
            <button onClick={() => dispatch(division())}>나눗셈</button>
            <p>
                결과값: {result}
            </p>
        </div>
    )
}