import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import ReduxEx from './ReduxEx';

// Provider 컴포넌트에 store 속성에 생성한 store 주입
// 해당 컴포넌트로 감싸인 컴포넌트들은 스토어 사용이 가능
export default function ReduxIndex() {
    return (
        <Provider store={store}>
            <ReduxEx />
        </Provider>
    )
}