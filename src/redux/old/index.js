import {combineReducers} from 'redux';
import counter from './oldCounter';

export const rootReducer = combineReducers({
    counter,
});
