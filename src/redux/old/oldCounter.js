// 기존 리덕스

// action type
const ADD = 'ADD';

// action creator
export const addNumber = (payload) => {
    return {
        type: ADD,
        payload,
    };
};

// Initial State
const initialState = {
    number: 0,
};

// Reducer
const counter = (state = initialState, action) => {
    switch (action.type) {
        case ADD:
            return {
                ...state,
                number: state.number + action.payload
            };
        default:
            return state;
    }
};
export default counter;