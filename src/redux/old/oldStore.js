import {createStore} from 'redux';
import {rootReducer} from './index';
import {Provider} from 'react-redux';

const store = createStore(rootReducer);

// 이런 형식으로 사용
// <Provider store={store}>
//     <App/>
// </Provider>