import React, {useEffect, useState} from 'react';
import usePost from './hooks/usePost';

export default function App2() {

    const [me, setMe] = useState(null);

    // 커스텀 훅으로 아래 API 코드 재사용
    const {posts, loadPosts, createPosts, updatePosts, deletePosts} = usePost();

    // 데이터 요청
    useEffect(() => {
        // fetch 메서드를 활용하여 get 요청을 하였습니다.
        // fetch 는 HTTP request 기능을 제공하는 클라이언트단 Web API 입니다.
        // 비동기로 처리되며, promise 기반 입니다.
        fetch('http://localhost:3004/me')
            .then(res => res.json())
            .then(result => {
                console.log(result);
                setMe(result); // 서버에서 응답받은 데이터를 상태값에 저장
            }).catch(err => console.error(err));
    }, []);

    return (
        <div>
            <h2>App2 컴포넌트</h2>
            {me !== null &&
                <p>
                    {me.name} / {me.age} / {me.hobby}
                </p>
            }
            <button onClick={loadPosts}>
                posts 불러오기
            </button>
            <button onClick={createPosts}>
                POST: posts 추가
            </button>
            <button onClick={updatePosts}>
                PUT: posts 수정
            </button>
            <button onClick={deletePosts}>
                DELETE: posts 삭제
            </button>
            <div>
                {posts.length > 0 && posts.map(post => <p>{JSON.stringify(post)}</p>)}
            </div>
        </div>
    );
}
