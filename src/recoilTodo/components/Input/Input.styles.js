import styled from 'styled-components';

export const InputStyled = styled.input`
  
  outline: none;
  border: 1px solid transparent;
  font-size: 20px;
  letter-spacing: -0.5px;
  padding: 10px 16px;
  width: 100%;
  font-weight: 300;
  color: #262626;
  
  &:focus {
    border: 1px solid #4086ff;
  }

  input::-webkit-input-placeholder {
    color: #e6e6e6;
    opacity: 0.5;
  }
`;