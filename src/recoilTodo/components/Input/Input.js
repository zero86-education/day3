import React from 'react';
import {InputStyled} from './Input.styles';

export default function Input({value, onChange, placeholder}) {

    return (
        <InputStyled
            value={value}
            onChange={onChange}
            placeholder={placeholder}
            autoFocus
        />
    );
};
