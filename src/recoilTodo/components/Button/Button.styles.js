import styled from 'styled-components';

export const ButtonStyled = styled.button`
  font-size: 20px;
  width: 100%;
  padding: 10px 16px;
  letter-spacing: -0.5px;
  border: none;
  background-color: #4086ff;
  color: #f7f7f7;
  border-radius: 8px;
  margin: 8px 0;
  cursor: pointer;

  &:hover {
    background-color: #2d79fc;
  }
`;