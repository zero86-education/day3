import React, {useState} from 'react';
import {useRecoilState} from 'recoil';
import {todoState} from '../../store/atoms/todoAtoms';
import {v4 as uuidv4} from 'uuid';
import Input from '../Input/Input';
import Button from '../Button/Button';

export default function TodoInputForm() {
    const [todo, setTodo] = useRecoilState(todoState);

    const [text, setText] = useState('');
    const handleChange = (e) => {
        setText(e.target.value);
    };

    const handleClick = () => {
        console.log(text);
        if (text.trim() === '') {
            alert('잘못된 입력입니다.');
            return false;
        }
        // Recoil Todo Add
        setTodo([
            ...todo, {
                id: uuidv4(),
                text,
                isChecked: false,
            }
        ]);

        setText('');
    };

    return (
        <>
            <Input value={text} onChange={handleChange} placeholder="할 일을 입력해주세요.."/>
            <Button onClick={handleClick} text="Todo Add"/>
        </>
    );
}