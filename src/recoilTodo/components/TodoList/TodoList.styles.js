import styled from 'styled-components';

export const ListContainerStyled = styled.ul`
    
`;

export const ListItemStyled = styled.li`
  padding: 10px;
  letter-spacing: -0.5px;
  color: #282c34;
  display: flex;
  align-items: center;
  justify-content: space-between;

  input[type='checkbox']:checked {
    & + span {
      text-decoration: line-through;
    }
  }

`;

export const ListItemLabelStyled = styled.label`
  display: flex;
  align-items: center;
  min-width: 0;
`;

export const ListItemTodoTextStyled = styled.span`
  display: inline-block;
  padding-left: 10px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;