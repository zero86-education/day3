import React from 'react';
import {useRecoilState} from 'recoil';
import {todoState} from '../../store/atoms/todoAtoms';
import {ListContainerStyled, ListItemLabelStyled, ListItemStyled, ListItemTodoTextStyled} from './TodoList.styles';
import DeleteIcon from '../DeleteIcon/DeleteIcon';

export default function TodoList() {

    const [todo, setTodo] = useRecoilState(todoState);
    console.log(todo);

    const toggleCheckedTodo = (e, targetId) => {
        const {checked} = e.target;
        const updatedTodo = todo.map(todoItem => {
            if (todoItem.id === targetId) {
                return {
                    ...todoItem,
                    isChecked: checked
                };
            }
            return todoItem;
        });
        setTodo(updatedTodo);
    };

    const handleDeleteTodoClick = (targetId) => {
        const deletedTodo = todo.filter(todoItem => {
            if (todoItem.id !== targetId) return todoItem;
        });
        setTodo(deletedTodo);
    };

    return (
        <>
            {todo.length === 0 ?
                <p style={{textAlign: 'center'}}>추가 된 할 일이 없습니다 :)</p> :
                <ListContainerStyled>
                    {todo.map(todoItem => {
                        return (
                            <ListItemStyled>
                                <ListItemLabelStyled>
                                    <input type="checkbox" checked={todoItem.isChecked}
                                           onChange={(e) => toggleCheckedTodo(e, todoItem.id)}/>
                                    <ListItemTodoTextStyled>{todoItem.text}</ListItemTodoTextStyled>
                                </ListItemLabelStyled>
                                <DeleteIcon onClick={() => handleDeleteTodoClick(todoItem.id)}/>
                            </ListItemStyled>
                        );
                    })}
                </ListContainerStyled>}
        </>
    );
}