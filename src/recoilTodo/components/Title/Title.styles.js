import styled from 'styled-components';

export const TitleStyled = styled.h2`
  padding: 10px 0;
  font-size: 48px;
  text-align: center;
  color: #4086ff;
  font-weight: 600;
`;