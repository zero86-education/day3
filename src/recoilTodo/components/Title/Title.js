import React from 'react';
import {TitleStyled} from './Title.styles';

export default function Title({text = ''}) {
    return (
        <TitleStyled>
            {text}
        </TitleStyled>
    );
}