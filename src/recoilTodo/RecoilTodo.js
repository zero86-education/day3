import React from 'react';
import {RecoilRoot} from 'recoil';
import {ContainerStyled} from './RecoilTodo.styles';
import Title from './components/Title/Title';
import TodoInputForm from './components/TodoInputForm/TodoInputForm';
import TodoList from './components/TodoList/TodoList';


export default function RecoilTodo() {
    return (
        <RecoilRoot>
            <ContainerStyled>
                <Title text="Recoil Todo"/>
                <TodoInputForm/>
                <TodoList/>
            </ContainerStyled>
        </RecoilRoot>
    );
}