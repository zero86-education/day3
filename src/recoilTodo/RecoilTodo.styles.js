import styled from 'styled-components';

export const ContainerStyled = styled.section`
  width: 100%;
  max-width: 800px;
  margin: 0 auto;
  padding: 8px;
`;