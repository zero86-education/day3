import React, {useEffect, useState} from 'react';
// import usePost from './hooks/usePost';

// 실행 시, npm run json-server 해주기
export default function App() {

    const [me, setMe] = useState(null);
    const [posts, setPosts] = useState([]);

    // 커스텀 훅으로 아래 API 코드 재사용
    // const {posts, loadPosts, createPosts, updatePosts, deletePosts} = usePost();

    // 데이터 요청
    useEffect(() => {
        // fetch 메서드를 활용하여 get 요청을 하였습니다.
        // fetch 는 HTTP request 기능을 제공하는 클라이언트단 Web API 입니다.
        // 비동기로 처리되며, promise 기반 입니다.
        fetch('http://localhost:3004/me')
            .then(res => res.json())
            .then(result => {
                console.log(result);
                setMe(result); // 서버에서 응답받은 데이터를 상태값에 저장
            }).catch(err => console.error(err));
    }, []);

    const loadPosts = async () => {
        let result;
        try {
            const res = await fetch('http://localhost:3004/posts');
            result = await res.json();
        } catch (err) {
            console.error(err);
        }
        console.log(result);
        setPosts(result);
    };

    const createPosts = async () => {
        const newData = {
            title: '추가한 post',
            author: '안드로이드'
        };
        try {
            const res = await fetch('http://localhost:3004/posts', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newData),
            });
            if (res.status === 201) await loadPosts();
        } catch (err) {
            console.error(err);
        }
    };

    const updatePosts = async () => {
        // id가 13인 post 수정
        const updatedId = 13;
        try {
            const res = await fetch(`http://localhost:3004/posts/${updatedId}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    title: '수정한 post',
                    author: '수정한 author'
                }),
            });
            if (res.status === 200) await loadPosts();
        } catch (err) {
            console.error(err);
        }
    };

    const deletePosts = async () => {
        // id가 13인 post 삭제
        const deletedId = 13;
        try {
            const res = await fetch(`http://localhost:3004/posts/${deletedId}`, {
                method: 'DELETE',
            });
            if (res.status === 200) await loadPosts();
        } catch (err) {
            console.error(err);
        }
    };

    return (
        <div>
            <h2>App 컴포넌트</h2>
            {me !== null &&
                <p>
                    {me.name} / {me.age} / {me.hobby}
                </p>
            }
            <button onClick={loadPosts}>
                posts 불러오기
            </button>
            <button onClick={createPosts}>
                POST: posts 추가
            </button>
            <button onClick={updatePosts}>
                PUT: posts 수정
            </button>
            <button onClick={deletePosts}>
                DELETE: posts 삭제
            </button>
            <div>
                {posts.length > 0 && posts.map(post => <p>{JSON.stringify(post)}</p>)}
            </div>
        </div>
    );
}
