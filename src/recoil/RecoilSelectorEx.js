import React from 'react';
import {useRecoilValue} from 'recoil';
import {doubleCountSelector} from './atoms/countAtoms';

export default function RecoilSelectorEx() {
    const doubleCount = useRecoilValue(doubleCountSelector);

    return (
        <h2>
            double count: {doubleCount}
        </h2>
    )
}