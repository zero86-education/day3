import React from 'react';
import {RecoilRoot} from 'recoil';
import RecoilEx from './RecoilEx'; // recoil 을 사용하려면 <RecoilRoot> 컴포넌트로 감싸야 한다.
export default function RecoilIndex() {
    return(
        <RecoilRoot>
            <RecoilEx />
        </RecoilRoot>
    )
}