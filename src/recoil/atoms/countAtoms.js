import {atom, selector} from 'recoil';


// atoms
export const countState = atom({
    key: 'countState', // key 값은 유니크 해야 합니다.
    default: 0, // 초기 상태값 지정
});

// selector
export const doubleCountSelector = selector({
    key: 'doubleCountSelector',
    get: ({ get }) => {
        const count = get(countState);
        return count * 2;
    }
})