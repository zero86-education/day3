import React from 'react';
import {useRecoilState, useRecoilValue} from 'recoil';
import {countState} from './atoms/countAtoms';
import RecoilSelectorEx from './RecoilSelectorEx';


const ChildComponent = () => {
    const count = useRecoilValue(countState);
    return (
        <div>
            <h2>
                {count}
            </h2>
        </div>
    );
};

export default function RecoilEx() {

    const [count, setCount] = useRecoilState(countState);
    const handleClick = () => {
        setCount(count + 1);
    };

    return (
        <div>
            <p>
                {count}
            </p>
            <button onClick={handleClick}>
                Increment
            </button>
            <ChildComponent/>
            <RecoilSelectorEx />
        </div>
    );
}