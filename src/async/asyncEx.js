// async & await
function requestData(value) {
    // Promise 반환
    return new Promise(resolve => {
        setTimeout(() => {
            console.log('requestData', value);
            resolve(value);
        }, 1000);
    });
}

async function getData() {
    const data1 = await requestData(10);
    const data2 = await requestData(777);
    console.log(data1, data2); // 10 777
};
getData();


// async 함수 반환값은 Promise
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function process() {
    console.log('안녕하세요!');
    await sleep(1000); // 1초쉬고
    console.log('반갑습니다!');
}

process().then(() => {
    console.log('작업이 끝났어요!');
});


// error 처리
const errTest1 = async () => {
    console.log(bbb);
};

const getResult = async () => {
    try {
        const errTestResult = await errTest1();
        console.log(errTestResult);
    } catch (err) {
        console.log(err);
    }
};
getResult();