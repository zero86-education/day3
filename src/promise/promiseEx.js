// 프로미스 생성
const promise = new Promise((resolve, reject) => {

});

// 프로미스 성공
const myPromise = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('success');
    }, 1000);
});
myPromise.then((result) => console.log(result));


// 프로미스 대기
const promisePending = new Promise(() => {

});
console.log(promisePending); // [[PromiseState]]: "pending"

// 프로미스 성공(이행)
function getDataSuccess() {
    return new Promise((resolve, reject) => {
        const data = 100;
        resolve(data);
    });
}

// resolve()의 결과 값 data를 resolvedData로 받음
getDataSuccess().then(resolvedData => {
    console.log(resolvedData); // 100
});

// 프로미스 실패
function getDataFail() {
    return new Promise((resolve, reject) => {
        reject(new Error('Request is failed'));
    });
}

// reject()의 결과 값 Error를 err에 받음
getDataFail().then().catch(err => {
    console.log(err); // Error: Request is failed
});


// finally
function getUser() {
    return new Promise(resolve => resolve('getUser success'));
};

getUser().then(result => console.log(result)).finally(() => {
    // logging
    console.log('log getUser()');
});

// 프로미스 체이닝
new Promise((resolve, reject) => {
    setTimeout(function () {
        resolve(1);
    }, 2000);
})
    .then(result => {
        console.log(result); // 1
        return result + 10;
    })
    .then(result => {
        console.log(result); // 11
        return result + 20;
    })
    .then(result => {
        console.log(result); // 31
    });

// 프로미스 에러 핸들링
new Promise((resolve, reject) => {
    throw new Error('에러 발생');
}).catch(err => console.error(err));

new Promise((resolve, reject) => {
    reject(new Error('에러 발생!!'));
}).catch(err => console.error(err));

new Promise((resolve, reject) => {
    resolve('success');
}).then(result => {
   undefinedFunc(); // 존재하지 않는 함수
}).catch(err => console.error(err));


// Promise.all()
function success80percentPromise() {
    return new Promise((resolve, reject) => {
        const value = Math.random()
        if (value >= 0.2) {
            return resolve(value)
        }
        return reject(value); // 20% 확률로 실패
    })
}

function fulfilledPromises() {
    const promises = Array(10)
        .fill(0)
        .map(() => success80percentPromise());

    Promise.all(promises)
        .then(result => console.log(result))
        .catch(err => console.error('error!!', err));
}
fulfilledPromises();

// Promise.allSettled()
function success90percentPromise () {
    return new Promise((resolve, reject) => {
        const value = Math.random()
        if (value >= 0.1) {
            return resolve(value)
        }
        return reject(value)
    })
};

function allSettledPromises () {
    const promises = Array(10).fill(0).map(() => success90percentPromise())
    Promise.allSettled(promises)
        .then(result => console.log(result))
        .catch(err => console.error(err));
};
allSettledPromises();