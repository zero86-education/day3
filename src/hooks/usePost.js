import {useState} from 'react';

export default function usePost() {
    const [posts, setPosts] = useState([]);

    const loadPosts = async () => {
        let result;
        try {
            const res = await fetch('http://localhost:3004/posts');
            result = await res.json();
        } catch (err) {
            console.error(err);
        }
        console.log(result);
        setPosts(result);
    };

    const createPosts = async () => {
        const newData = {
            title: '추가한 post',
            author: '안드로이드'
        };
        try {
            const res = await fetch('http://localhost:3004/posts', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newData),
            });
            if (res.status === 201) await loadPosts();
        } catch (err) {
            console.error(err);
        }
    };

    const updatePosts = async () => {
        // id가 13인 post 수정
        const updatedId = 13;
        try {
            const res = await fetch(`http://localhost:3004/posts/${updatedId}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    title: '수정한 post',
                    author: '수정한 author'
                }),
            });
            if (res.status === 200) await loadPosts();
        } catch (err) {
            console.error(err);
        }
    };

    const deletePosts = async () => {
        // id가 13인 post 삭제
        const deletedId = 13;
        try {
            const res = await fetch(`http://localhost:3004/posts/${deletedId}`, {
                method: 'DELETE',
            });
            if (res.status === 200) await loadPosts();
        } catch (err) {
            console.error(err);
        }
    };

    return {
        posts,
        loadPosts,
        createPosts,
        updatePosts,
        deletePosts
    };
}